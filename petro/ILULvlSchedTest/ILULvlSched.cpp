// STD
#include <cstdlib>
#include <vector>

// Boost
#include <boost/shared_ptr.hpp>

// SolverBR
#include <parallel/utils/ParallelUtils.h>
#include <performance/kernel/ILULvlSched.h>
#include <performance/timer/GlobalTimerTree.h>
#include <performance/timer/TimerTree.h>
#include <solver/linear/core/InnerLocalPartition.h>
#include <solver/linear/core/LocalMatrix.h>
#include <solver/linear/core/LocalVector.h>
#include <solver/linear/core/Matrix.h>
#include <solver/linear/core/Partition.h>
#include <solver/linear/core/PartitionFactory.h>
#include <solver/linear/core/Vector.h>
#include <solver/linear/solver/KrylovSolverCG.h>
#include <solver/linear/solver/permuter/LevelSchedulingPermuter.h>
#include <solver/linear/solver/permuter/Permuter.h>
#include <solver/linear/solver/preconditioner/GenericDDLvlSchedPreconditionerFactory.h>
#include <solver/linear/solver/preconditioner/ILUCoreBase.h>
#include <solver/linear/storage/csr/CSRInnerLocalMatrix.h>
#include <solver/linear/storage/dense/DenseInnerLocalVector.h>
#include <tools/laplacian/LaplacianBuilder.h>
#include <utils/assurance/AssertUtils.h>

using performance::timer::GlobalTimerTree;
using performance::timer::TimerTree;
using solver::linear::core::InnerLocalPartition;
using solver::linear::core::Matrix;
using solver::linear::core::LocalMatrix;
using solver::linear::core::LocalVector;
using solver::linear::core::Partition;
using solver::linear::core::PartitionFactory;
using solver::linear::core::Vector;
using solver::linear::solver::KrylovSolverCG;
using solver::linear::solver::permuter::LevelSchedulingPermuter;
using solver::linear::solver::permuter::Permuter;
using solver::linear::solver::preconditioner::GenericDDLvlSchedPreconditionerFactory;
using solver::linear::solver::preconditioner::ILUCoreBase;
using solver::linear::storage::csr::CSRInnerLocalMatrix;
using solver::linear::storage::dense::DenseInnerLocalVector;
using tools::laplacian::LaplacianBuilder;

namespace {
   boost::shared_ptr<Partition> CreatePartition( size_t n, int nDomains )
   {
      SharedArray<int> localPartitionNDomains( std::move( SharedArray<int>::Factory::Allocate::Instance( 1 ) ) );
      SharedArray<int> localPartitionSizes( std::move( SharedArray<int>::Factory::Allocate::Instance( 1 ) ) );
      SharedArray<int> localPartitionsOwnerRanks( std::move( SharedArray<int>::Factory::Allocate::Instance( 1 ) ) );

      OpenMP::MasterBlocking( [&] ()
      {
         localPartitionNDomains.first() = nDomains;
         localPartitionSizes.first() = n * n * n;
         localPartitionsOwnerRanks.first() = MPI::Rank();
      } );

      return PartitionFactory::Create( localPartitionNDomains, localPartitionSizes, localPartitionsOwnerRanks );
   }

   /*
    * This implementation of a 27 Points 3D Laplacian matrix generation follows the code present in
    * https://github.com/IntelLabs/SpMP/blob/master/Laplacian.cpp
    */
   boost::shared_ptr<LocalMatrix> CreateCSRInnerLocalMatrix27PtLaplacian( size_t n, const boost::shared_ptr<LocalPartition>& localPartition )
   {
      constexpr double diagVal = 26.0;
      constexpr double offdiagVal = -1.0;

      boost::shared_ptr<InnerLocalPartition> innerLP = boost::dynamic_pointer_cast<InnerLocalPartition>( localPartition );
      CK_TRUE( innerLP );

      SharedArray<int64> rowIndex = std::move( SharedArray<int64>::Factory::Allocate::Instance( n * n * n + 1 ) );
      int nx = n, ny = n, nz = n;

      OpenMP::MasterBlocking( [&] ()
      {
         rowIndex.first() = 0;
         int64 idx = 0;

         for ( int iz = 0; iz < nz; ++iz )
         {
            int dz = ( iz >= 1 && iz < nz - 1 ) ? 3 : ( nz == 1 ? 1 : 2 );
            for ( int iy = 0; iy < ny; ++iy )
            {
               int dy = ( ( iy >= 1 && iy < ny - 1 ) ? 3 : ( nz == 1 ? 1 : 2 ) ) * dz;
               for ( int ix = 0; ix < nx; ++ix )
               {
                  int row = ( iz * ny + iy ) * nx + ix;

                  int dx = ( ix >= 1 && ix < nx - 1 ) ? 3 : ( nz == 1 ? 1 : 2 );
                  idx += dx * dy;
                  rowIndex[ row + 1 ] = idx;
               }
            }
         }
      } );

      const int64 nnz = rowIndex.last();
      SharedArray<int> colIndex = std::move( SharedArray<int>::Factory::Allocate::Instance( nnz ) );
      SharedArray<double> values = std::move( SharedArray<double>::Factory::Allocate::Instance( nnz ) );

      OpenMP::MasterBlocking( [&] ()
      {
         int * ccolIndex = colIndex.begin();
         double * vvalues = values.begin();
         const int64 * rrowIndex = rowIndex.begin();

         for ( int iz = 0; iz < nz; ++iz )
         {
            for ( int iy = 0; iy < ny; ++iy )
            {
               int sz_begin = iz <= 0 ? 0 : -1;
               int sz_end = iz >= nz - 1 ? 0 : 1;

               int sy_begin = iy <= 0 ? 0 : -1;
               int sy_end = iy >= ny - 1 ? 0 : 1;
               for ( int ix = 0; ix < nx; ++ix )
               {
                  int sx_begin = ix <= 0 ? 0 : -1;
                  int sx_end = ix >= nx - 1 ? 0 : 1;

                  int row = ( iz * ny + iy ) * nx + ix;
                  int64 idx = rrowIndex[ row ];

                  for ( int sz = sz_begin; sz <= sz_end; ++sz )
                  {
                     for ( int sy = sy_begin; sy <= sy_end; ++sy )
                     {
                        for ( int sx = sx_begin; sx <= sx_end; ++sx )
                        {
                           int col = row + ( sz * ny + sy ) * nx + sx;
                           ccolIndex[ idx ] = col;

                           const double val = row == col ? diagVal : offdiagVal;
                           vvalues[ idx ] = val;
                           ++idx;
                        }
                     }
                  }
               }
            }
         }
      } );

      const int nRows = n * n * n;
      const int nColumns = n * n * n;
      const int64 nEntries = nnz;

      return boost::make_shared<CSRInnerLocalMatrix>( innerLP, innerLP, rowIndex, colIndex, values );
   }

   boost::shared_ptr<LocalMatrix> CreateCSRInnerLocalMatrix7PtLaplacian( size_t n, const boost::shared_ptr<LocalPartition>& localPartition )
   {
      constexpr double diagVal = 6.0;
      constexpr double offdiagVal = -1.0;

      boost::shared_ptr<InnerLocalPartition> innerLP = boost::dynamic_pointer_cast<InnerLocalPartition>( localPartition );
      CK_TRUE( innerLP );

      SharedArray<int64> rowIndex = std::move( SharedArray<int64>::Factory::Allocate::Instance( n * n * n + 1 ) );
      std::vector<int> ja;

      OpenMP::MasterBlocking( [&] ()
      {
         rowIndex.first() = 0;

         for ( int k = 0; k < n; ++k )
         {
            for ( int j = 0; j < n; ++j )
            {
               for ( int i = 0; i < n; ++i )
               {
                  int offset = k * n * n + j * n;
                  int offsetj = k * n * n + ( j - 1 ) * n;
                  int offsetjj = k * n * n + ( j + 1 ) * n;
                  int offsetk = ( k - 1 ) * n * n + j * n;
                  int offsetkk = ( k + 1 ) * n * n + j * n;

                  std::vector<int> cols;

                  if ( k - 1 >= 0 ) cols.push_back( offsetk + i );
                  if ( j - 1 >= 0 ) cols.push_back( offsetj + i );
                  if ( i - 1 >= 0 ) cols.push_back( offset + i - 1 );

                  const int row = offset + i;
                  cols.push_back( offset + i );

                  if ( i + 1 < n ) cols.push_back( offset + i + 1 );
                  if ( j + 1 < n ) cols.push_back( offsetjj + i );
                  if ( k + 1 < n ) cols.push_back( offsetkk + i );
                  
                  rowIndex[ row + 1 ] = rowIndex[ row ] + cols.size();
                  ja.insert( ja.end(), cols.begin(), cols.end() );
               }
            }
         }
      } );

      const int64 nnz = rowIndex.last();
      SharedArray<int> colIndex = std::move( SharedArray<int>::Factory::Allocate::Instance( nnz ) );
      SharedArray<double> values = std::move( SharedArray<double>::Factory::Allocate::Instance( nnz ) );

      OpenMP::MasterBlocking( [&] ()
      {
         const int * jja = ja.data();
         int * ccolIndex = colIndex.begin();
         double * vvalues = values.begin();
         const int64 * rrowIndex = rowIndex.begin();

         for ( int i = 0; i < n * n * n; ++i )
         {
            for ( auto idx = rrowIndex[ i ]; idx < rrowIndex[ i + 1 ]; ++idx )
            {
               int j = jja[ idx ];
               ccolIndex[ idx ] = j;
               vvalues[ idx ] =  i == j ? diagVal : offdiagVal;
            }
         }
      } );

      return boost::make_shared<CSRInnerLocalMatrix>( innerLP, innerLP, rowIndex, colIndex, values );
   }

   boost::shared_ptr<Matrix> CreateMatrix( size_t n, const boost::shared_ptr<Partition>& partition, const bool use27Pt )
   {
      const boost::shared_ptr<LocalPartition>& iLP = partition->localPartitions()[ 0 ];
      boost::shared_ptr<LocalMatrix> localMatrix = use27Pt ? std::move( CreateCSRInnerLocalMatrix27PtLaplacian( n, iLP ) ) : std::move( CreateCSRInnerLocalMatrix7PtLaplacian( n, iLP ) );
      CK_TRUE( localMatrix );

      std::vector< boost::shared_ptr< LocalMatrix> > localMatrices( 1, localMatrix );
      return boost::make_shared<Matrix>( partition, localMatrices );
   }

   boost::shared_ptr<DenseInnerLocalVector> CreateDenseInnerLocalVector( size_t n, const boost::shared_ptr<LocalPartition>& localPartition, double value )
   {
      boost::shared_ptr<InnerLocalPartition> innerLP = boost::dynamic_pointer_cast<InnerLocalPartition>( localPartition );
      CK_TRUE( innerLP );

      boost::shared_ptr< SharedArray<double> > values = std::move( make_shared< SharedArray<double> >( SharedArray<double>::Factory::Allocate::Instance( n * n * n ) ) );
      OpenMP::MasterBlocking( [&] ()
      {
         double * v = values->begin();
         for ( int i = 0; i < n * n * n; ++i )
         {
            v[ i ] = value;
         }
      } );

      return boost::make_shared<DenseInnerLocalVector>( innerLP, values );
   }

   boost::shared_ptr<Vector> CreateVector( size_t n, const boost::shared_ptr<Partition>& partition, double value )
   {
      const boost::shared_ptr<LocalPartition>& iLP = partition->localPartitions()[ 0 ];
      boost::shared_ptr<LocalVector> localVector = std::move( CreateDenseInnerLocalVector( n, iLP, value ) );
      CK_TRUE( localVector );

      std::vector<boost::shared_ptr<LocalVector> > localVectors( 1, localVector );
      return boost::make_shared<Vector>( partition, localVectors );
   }
}

namespace performance {
   namespace kernel {

      const char* ILULvlSched::KEY = "ILU-Lvl-Sched";

      ILULvlSched::ILULvlSched( boost::property_tree::ptree& input, boost::property_tree::ptree& globals ) :
         Kernel( KEY, input, globals )
      {
         CK_EQU( m_nTrials, 1 );
         CK_EQU( MPI::Size(), 1 );

         m_params.appendColumn( "runId", "Run ID" );
         m_params.appendColumn( "nXYZ", "# equations per axis" );
         m_params.appendColumn( "totalNDomains", "# domains total" );
         m_params.appendColumn( "fillLevel", "Fill Level" );
         m_params.appendColumn( "blocked", "Use blocked storage" );
         m_params.appendColumn( "isP2P", "Enable or disable P2P level scheduling" );
         m_params.appendColumn( "use27Pt", "Enable or disable use of 27 Points Laplacian Matrix" );
         m_params.appendColumn( "testKey", "Choose to test numeric factorization" );
         
         appendTimeStatisticsToParams();

         const int fillLevel = kernelParam<int>( "fillLevel" );   // The input value 'fillLevel'
         const bool blocked = kernelParam<bool>( "blocked" );   // The input value 'nonzero'
         const bool isP2P = kernelParam<bool>( "isP2P" );
         const bool use27Pt = kernelParam<bool>( "use27Pt" );
         const std::string testKey = kernelParam<std::string>( "testKey" );

         m_nSamples = kernelParam<int>( "nSamples", 1 );

         int runId = 0;
         for ( size_t nXYZ : kernelArrayParam<size_t>( "nXYZ" ) )
         {
            for ( int nThreads : nThreadsArray() )
            {
               int totalNDomains = nThreads;

               m_params.appendRow();
               m_params.field( "runId"             ) = runId++;
               m_params.field( "kernel"            ) = m_name;
               m_params.field( "nProcs"            ) = 1;
               m_params.field( "nThreads"          ) = nThreads;
               m_params.field( "totalNDomains"     ) = totalNDomains;
               m_params.field( "nSamples"          ) = m_nSamples;
               m_params.field( "nTrials"           ) = m_nTrials;
               m_params.field( "affinity"          ) = m_affinity;
               m_params.field( "nXYZ"              ) = nXYZ;
               m_params.field( "fillLevel"         ) = fillLevel;
               m_params.field( "blocked"           ) = blocked;
               m_params.field( "isP2P"             ) = isP2P;
               m_params.field( "use27Pt"           ) = use27Pt;
               m_params.field( "testKey"           ) = testKey;
            }
         }
      }

      void ILULvlSched::execute( const std::string& outFilename )
      {
         for ( size_t r = 0; r < m_params.nBodyRows(); ++r )
         {
            int                runId = m_params.field( r, "runId"             ).get<int   >();
            int               nProcs = m_params.field( r, "nProcs"            ).get<int   >();
            int             nThreads = m_params.field( r, "nThreads"          ).get<int      >();
            size_t              nXYZ = m_params.field( r, "nXYZ"              ).get<size_t   >();
            int        totalNDomains = m_params.field( r, "totalNDomains"     ).get<int      >();
            int            fillLevel = m_params.field( r, "fillLevel"         ).get<int      >();
            bool             blocked = m_params.field( r, "blocked"           ).get<bool     >();
            bool             isP2P   = m_params.field( r, "isP2P"             ).get<bool     >();
            bool             use27Pt = m_params.field( r, "use27Pt"           ).get<bool     >();
            std::string      testKey = m_params.field( r, "testKey"        ).get<std::string  >();

            OpenMP::ParallelN( nThreads, [&]()
            {
               TimerTree& timer = GlobalTimerTree::Instance();

               // Reset timer to the new run
               timer.reset();

               timer.push( "Kernel: ILULvlSched" );

               boost::shared_ptr<Partition> partition;
               boost::shared_ptr<Matrix> pA;
               boost::shared_ptr<Vector> px;
               boost::shared_ptr<Vector> py;
               boost::shared_ptr<Vector> pu;

               TimerTree::Time( timer, "Laplacian", [&]() // Using a different interface for push/pop sub-timers
               {
                  TimerTree::Time( timer, "Create Partition",             [&](){ partition = std::move( CreatePartition( nXYZ, nThreads ) ); } );
                  TimerTree::Time( timer, "Create Matrix A",              [&](){ pA = std::move( CreateMatrix( nXYZ, partition, use27Pt ) );          } );
                  TimerTree::Time( timer, "Create Vector x",              [&](){ px = std::move( CreateVector( nXYZ, partition, 0.01 ) );     } );
                  TimerTree::Time( timer, "Create Vector y (clone of x)", [&](){ py.reset( px->clone( true ) );                             } );
                  TimerTree::Time( timer, "Create Vector u (clone of x)", [&](){ pu.reset( px->clone( false ) );                             } );
               } );

               Matrix& A = *pA;
               Vector& x = *px;
               Vector& y = *py;
               Vector& u = *pu;

               boost::shared_ptr< GenericDDLvlSchedPreconditionerFactory > genDDLvlSchedPrecFactory( new GenericDDLvlSchedPreconditionerFactory() );
               boost::shared_ptr< GenericDDLvlSchedPreconditioner > iluJacobiDDLvlSchedPrec = genDDLvlSchedPrecFactory->createILUJacobiDDLvlSchedPreconditionerWithSingleMultiDomainPerInnerPartition( pA );
               // TODO FIX set option on precoditioner
               iluJacobiDDLvlSchedPrec->setOption<int>( "Fill Level", fillLevel );
               iluJacobiDDLvlSchedPrec->setOption<bool>( "Enable P2P", isP2P );

               iluJacobiDDLvlSchedPrec->symbolicSetup();
               OpenMP::Barrier();
               boost::shared_ptr< Permuter > permuter( new LevelSchedulingPermuter( *iluJacobiDDLvlSchedPrec ) );
//               TimerTree::Time( timer, "Permute A", [&](){ permuter->permuteMatrix( A ); } );
               TimerTree::Time( timer, "Permute y", [&](){ permuter->permuteVector( y, Permuter::TO_PERMUTATION_ORDER ); } );
//
//               iluJacobiDDLvlSchedPrec->setOperator( pA );
               iluJacobiDDLvlSchedPrec->setPermuter( permuter );
               iluJacobiDDLvlSchedPrec->numericSetup();

               CK_EQU( iluJacobiDDLvlSchedPrec->coreAlgorithms().size(), 1 );
               CK_EQU( iluJacobiDDLvlSchedPrec->matrixExtractors().size(), 1 );

               const auto& ilu = iluJacobiDDLvlSchedPrec->coreAlgorithms()[ 0 ];
               const auto& matExt = iluJacobiDDLvlSchedPrec->matrixExtractors()[ 0 ];
               const auto& iluBase = boost::dynamic_pointer_cast<ILUCoreBase>( ilu );

               const auto& xilv = boost::dynamic_pointer_cast<InnerLocalVector>( x.localVectors()[ 0 ] );
               const auto& yilv = boost::dynamic_pointer_cast<InnerLocalVector>( y.localVectors()[ 0 ] );
               const auto& uilv = boost::dynamic_pointer_cast<InnerLocalVector>( u.localVectors()[ 0 ] );

               const int64 n = nXYZ * nXYZ * nXYZ;

//               iluBase->luMatvecPub( yilv->pt( 0 ), xilv->pt( 0 ), n );
	       OPENMP( omp barrier );

               if ( testKey == std::string( NUM_FACT_TEST ) )
               {
                  for ( int s = 0; s < m_nSamples; ++s )
                  {
                     xilv->set( 0.0 ); // Otherwise, the initial guess is the exact solution.

	             OPENMP( omp barrier );
                     wallTimerStart();

	             OPENMP( omp barrier );
                     timer.push( "ILU NumFact" );

                     ilu->numericalSetup( matExt );

	             OPENMP( omp barrier );
                     timer.pop();

	             OPENMP( omp barrier );
                     wallTimerEnd( s );

                     // relative residue
//                     timer.push( "Relative residue" );
//                     uilv->set( 0.0 );
//	             OPENMP( omp barrier );
//                     iluBase->luMatvecPub( uilv->pt( 0 ), xilv->pt( 0 ), n );
//	             OPENMP( omp barrier );
//                     const auto uNorm = uilv->norm2();
//	             OPENMP( omp barrier );
//                     uilv->axpyUpdate( -1, *yilv );
//	             OPENMP( omp barrier );
//                     timer.pop();
//                     double relres = uilv->norm2() / ( yilv->norm2() > 1E-16 ? yilv->norm2() : 1 );

	             ilu->apply( xilv->pt( 0 ), yilv->pt( 0 ), n, blocked ? 3 : 1 );
                     LINF << "nSamples = " << m_nSamples;
                     LINF << "nThreads = " << OpenMP::NumThreads();
                     LINF << "x.norm2() = " << xilv->norm2();
//                     LINF << "y.norm2() = " << yilv->norm2();
//                     LINF << "u.norm2() = " << uNorm;
//                     LINF << "r.norm2() = " << uilv->norm2();
//                     LINF << "relative residue = " << relres;

//                     CK_TRUE( relres < 1e-6 );
                  }
               }
               else if ( testKey == std::string( ALU_INIT_TEST ) )
               {
                  for ( int s = 0; s < m_nSamples; ++s )
                  {
	             OPENMP( omp barrier );
                     wallTimerStart();

	             OPENMP( omp barrier );
                     timer.push( "ILU Alu Init" );

	             iluBase->aluInit( 0.1, blocked ? 3 : 1 );

	             OPENMP( omp barrier );
                     timer.pop();

	             OPENMP( omp barrier );
                     wallTimerEnd( s );

                     // relative residue
//                     timer.push( "Relative residue" );
//                     uilv->set( 0.0 );
//	             OPENMP( omp barrier );
//                     iluBase->luMatvecPub( uilv->pt( 0 ), xilv->pt( 0 ), n );
//	             OPENMP( omp barrier );
//                     const auto uNorm = uilv->norm2();
//	             OPENMP( omp barrier );
//                     uilv->axpyUpdate( -1, *yilv );
//	             OPENMP( omp barrier );
//                     timer.pop();
//                     double relres = uilv->norm2() / ( yilv->norm2() > 1E-16 ? yilv->norm2() : 1 );

                     LINF << "nSamples = " << m_nSamples;
                     LINF << "nThreads = " << OpenMP::NumThreads();
//                     LINF << "x.norm2() = " << xilv->norm2();
//                     LINF << "y.norm2() = " << yilv->norm2();
//                     LINF << "u.norm2() = " << uNorm;
//                     LINF << "r.norm2() = " << uilv->norm2();
//                     LINF << "relative residue = " << relres;

//                     CK_TRUE( relres < 1e-6 );
                  }
               }
               else if ( testKey == std::string( APPLY_TEST ) )
               {
                  for ( int s = 0; s < m_nSamples; ++s )
                  {
                     xilv->set( 0.0 ); // Otherwise, the initial guess is the exact solution.

	             OPENMP( omp barrier );
                     wallTimerStart();

	             OPENMP( omp barrier );
                     timer.push( "ILU apply" );

	             ilu->apply( xilv->pt( 0 ), yilv->pt( 0 ), n, blocked ? 3 : 1 );

	             OPENMP( omp barrier );
                     timer.pop();

	             OPENMP( omp barrier );
                     wallTimerEnd( s );

                     // relative residue
//                     timer.push( "Relative residue" );
//                     uilv->set( 0.0 );
//	             OPENMP( omp barrier );
//                     iluBase->luMatvecPub( uilv->pt( 0 ), xilv->pt( 0 ), n );
//	             OPENMP( omp barrier );
//                     const auto uNorm = uilv->norm2();
//	             OPENMP( omp barrier );
//                     uilv->axpyUpdate( -1, *yilv );
//	             OPENMP( omp barrier );
//                     timer.pop();
//                     double relres = uilv->norm2() / ( yilv->norm2() > 1E-16 ? yilv->norm2() : 1 );

                     LINF << "nSamples = " << m_nSamples;
                     LINF << "nThreads = " << OpenMP::NumThreads();
//                     LINF << "x.norm2() = " << xilv->norm2();
//                     LINF << "y.norm2() = " << yilv->norm2();
//                     LINF << "u.norm2() = " << uNorm;
//                     LINF << "r.norm2() = " << uilv->norm2();
//                     LINF << "relative residue = " << relres;

//                     CK_TRUE( relres < 1e-6 );
                  }
               }
               else
               {
                  throw Exception( "Not supported test: " + std::string( testKey ) );
               }

               timer.pop();

               // Only the Thread 0 of the Rank 0 is saving the TimerTree file (in order to reduce the number of files)
               Hybrid::Master( [&]()
               {
                  std::string timerTreeFilename = outFilename + ".NP" + std::to_string( MPI::Size() ) + ".NT" + std::to_string( OpenMP::NumThreads() ) + ".P" + std::to_string( MPI::Rank() ) + ".T" + std::to_string( OpenMP::ThreadId() ) + ".TimerTree_RunId" + std::to_string( runId );
                  boost::shared_ptr<Table> tab = timer.measurements();
                  tab->appendColumn( "nProcesses", "# Processes" );
                  tab->appendColumn( "nThreads", "# Threads" );
                  tab->appendColumn( "nTasks", "# Tasks" );
                  for ( int r = 0; r < tab->nBodyRows(); ++r )
                  {
                     tab->field( r, "nProcesses" ).set( MPI::Size() );
                     tab->field( r, "nThreads" ).set( OpenMP::NumThreads() );
                     tab->field( r, "nTasks" ).set( MPI::Size() * OpenMP::NumThreads() );
                  }
                  tab->exportToCsv( timerTreeFilename );
               } );
               OpenMP::Barrier();

            } );

            fillStatistics( r );
         }

         exportResults( outFilename );
      }

      ILULvlSched::~ILULvlSched() {}
   }
}

