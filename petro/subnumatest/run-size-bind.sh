#!/bin/bash

module load gcc/6.5
source /opt/intel/parallel_studio_xe_2018/compilers_and_libraries_2018.5.274/linux/bin/compilervars.sh intel64

prefix=$1
shift
suffix=$1
shift
size=$1
shift
bind1=$1
shift
testKey=$1
shift

export OMP_PLACES=cores
export OMP_PROC_BIND=$bind1

cd /petrobr/parceirosbr/solverbrict/joao.ramirez2/ilu-lvl-sched/singleMultidomain/intel/${suffix}

dirname="${prefix}-${size}-${bind1}-${testKey}-${suffix}"

if [ ! -d $dirname ]
then
   mkdir $dirname
fi

cd $dirname

if [ ! -f ILULvlSchedNumFactTest ]
then
   ln -s $(pwd)/../ILULvlSchedTestIntel18 ILULvlSchedTest
fi

if [ ! -f Kernels.json ]
then
   sed "s/\@\@size\@\@/${size}/g" $(pwd)/../Kernels.json.size | sed "s/\@\@testKey\@\@/${testKey}/g" | tee Kernels.json 
fi

time ./ILULvlSchedTest &> ${dirname}.log 
