// Boost
#include <boost/lexical_cast.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

// SolverBR
#include <performance/timer/GlobalTimerTree.h>
#include <performance/timer/TimerTree.h>
#include <solver/linear/solver/preconditioner/ILUCore.h>
#include <solver/linear/solver/preconditioner/LevelSchedulingUtils.h>
#include <solver/linear/solver/preconditioner/MatrixExtractorForILU.h>
#include <solver/linear/solver/preconditioner/OptionsUtils.h>
#include <solver/linear/solver/preconditioner/RowExtractorForILU.h>
#include <utils/debug/Developer.h>

using performance::timer::GlobalTimerTree;
using performance::timer::TimerTree;

namespace {
   template<typename PA>
   ALWAYS_INLINE void ApplyBarrierLevelScheduling( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * jColLevelScheduling, const PA * aluLevelScheduling,
      const int * taskBoundaries, const int n, const int nLevels, const int thread, const int nThreads,
      double * y, const double * x, TimerTree& timer )
   {
      // For ILU, applying operator is given by y = [ U^(-1) * L^(-1) ] * x
      // This is obtained by solving (LU)*y = x, which is done in two steps:
      // 1 - Solve L*w = x
      // 2 - Solve U*y = w

      // Solve L*w = x, w is stored in y (forward solution)
      timer.push( "ILUCore Solve (L * w = x)" );
      for ( int l = 0; l < nLevels; ++l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l + 1 ];

         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            PA v = static_cast<PA>( x[ i ] );

            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               const int j = jColLevelScheduling[ jIdx ];
               v -= aluLevelScheduling[ jIdx ] * static_cast<PA>( y[ j ] );
            }

            y[ i ] = static_cast<double>( v );
         }

         OpenMP::Barrier();
      }
      timer.pop();

      // Solve U*y = w (backward solution)
      timer.push( "ILUCore Solve (U * y = w)" );
      for ( int l = nLevels - 1; l >= 0; --l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l + 1 ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l ];

         for ( int i = taskBegin - 1; i >= taskEnd; --i )
         {
            PA v =static_cast<PA>( y[ i ] );

            for ( auto jIdx = uRowIndexLevelScheduling[ i ] + 1; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               const int j = jColLevelScheduling[ jIdx ];
               v -= aluLevelScheduling[ jIdx ] * static_cast<PA>( y[ j ] );
            }

            y[ i ] = static_cast<double>( aluLevelScheduling[ uRowIndexLevelScheduling[ i ] ] * v );
         }

         OpenMP::Barrier();
      }
      timer.pop();
   }

   template<typename PA>
   ALWAYS_INLINE void ApplyP2PLevelScheduling( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * jColLevelScheduling, const PA * aluLevelScheduling,
      const int * taskBoundaries, const int * parentIndexForward, const int * parentsForward,
      const int * parentIndexBackward, const int * parentsBackward,
      const int n, const int nLevels, const int thread, const int nThreads,
      double * y, const double * x, TimerTree& timer )
   {
      boost::shared_ptr< SharedArray< int > > taskFinishedForwardSharedArray;
      CreateTaskFinished( nLevels, nThreads, taskFinishedForwardSharedArray );
      volatile int * taskFinishedForward = taskFinishedForwardSharedArray->begin();

      // For ILU, applying operator is given by y = [ U^(-1) * L^(-1) ] * x
      // This is obtained by solving (LU)*y = x, which is done in two steps:
      // 1 - Solve L*w = x
      // 2 - Solve U*y = w

      // Solve L*w = x, w is stored in y (forward solution)
      timer.push( "ILUCore Solve (L * w = x)" );
      for ( int l = 0; l < nLevels; ++l )
      {
         const int task = thread * nLevels + l;
         const int taskBegin = taskBoundaries[ task ];
         const int taskEnd = taskBoundaries[ task + 1 ];

         PRAGMA( forceinline )
         P2PWait( l, parentIndexForward, parentsForward, taskFinishedForward );

         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            PA v = static_cast<PA>( x[ i ] );

            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               const int j = jColLevelScheduling[ jIdx ];
               v -= aluLevelScheduling[ jIdx ] * static_cast<PA>( y[ j ] );
            }

            y[ i ] = static_cast<double>( v );
         }

         PRAGMA( forceinline )
         P2PNotify( task, taskFinishedForward );
      }
      timer.pop();

      boost::shared_ptr< SharedArray< int > > taskFinishedBackwardSharedArray;
      CreateTaskFinished( nLevels, nThreads, taskFinishedBackwardSharedArray );
      volatile int * taskFinishedBackward = taskFinishedBackwardSharedArray->begin();

      // Solve U*y = w (backward solution)

      timer.push( "ILUCore Solve (U * y = w)" );
      for ( int l = nLevels - 1; l >= 0; --l )
      {
         const int task = thread * nLevels + l;
         const int taskBegin = taskBoundaries[ task + 1 ];
         const int taskEnd = taskBoundaries[ task ];

         PRAGMA( forceinline )
         P2PWait( l, parentIndexBackward, parentsBackward, taskFinishedBackward );

         for ( int i = taskBegin - 1; i >= taskEnd; --i )
         {
            PA v = static_cast<PA>( y[ i ] );

            for ( auto jIdx = uRowIndexLevelScheduling[ i ] + 1; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               const int j = jColLevelScheduling[ jIdx ];
               v -= aluLevelScheduling[ jIdx ] * static_cast<PA>( y[ j ] );
            }

            y[ i ] = static_cast<double>( aluLevelScheduling[ uRowIndexLevelScheduling[ i ] ] * v );
         }

         PRAGMA( forceinline )
         P2PNotify( task, taskFinishedBackward );
      }
      timer.pop();
   }

   template<typename PA>
   ALWAYS_INLINE void ApplyP2PLevelSchedulingUIdx( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * lJColLevelScheduling, const int * uJColLevelScheduling, const PA * lAluLevelScheduling, const PA * uAluLevelScheduling,
      const int * taskBoundaries, const int * parentIndexForward, const int * parentsForward,
      const int * parentIndexBackward, const int * parentsBackward,
      const int n, const int nLevels, const int thread, const int nThreads,
      double * y, const double * x, TimerTree& timer )
   {
      boost::shared_ptr< SharedArray< int > > taskFinishedForwardSharedArray;
      CreateTaskFinished( nLevels, nThreads, taskFinishedForwardSharedArray );
      volatile int * taskFinishedForward = taskFinishedForwardSharedArray->begin();

      // For ILU, applying operator is given by y = [ U^(-1) * L^(-1) ] * x
      // This is obtained by solving (LU)*y = x, which is done in two steps:
      // 1 - Solve L*w = x
      // 2 - Solve U*y = w

      // Solve L*w = x, w is stored in y (forward solution)
      timer.push( "ILUCore Solve (L * w = x)" );
      for ( int l = 0; l < nLevels; ++l )
      {
         const auto task = thread * nLevels + l;
         const auto taskBegin = taskBoundaries[ task ];
         const auto taskEnd = taskBoundaries[ task + 1 ];

         PRAGMA( forceinline )
         P2PWait( l, parentIndexForward, parentsForward, taskFinishedForward );

         for ( auto i = taskBegin; i < taskEnd; ++i )
         {
            PA v = static_cast<PA>( x[ i ] );

            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               const auto j = lJColLevelScheduling[ jIdx ];
               v -= lAluLevelScheduling[ jIdx ] * static_cast<PA>( y[ j ] );
            }

            y[ i ] = static_cast<double>( v );
         }

         PRAGMA( forceinline )
         P2PNotify( task, taskFinishedForward );
      }
      timer.pop();

      boost::shared_ptr< SharedArray< int > > taskFinishedBackwardSharedArray;
      CreateTaskFinished( nLevels, nThreads, taskFinishedBackwardSharedArray );
      volatile int * taskFinishedBackward = taskFinishedBackwardSharedArray->begin();

      // Solve U*y = w (backward solution)

      timer.push( "ILUCore Solve (U * y = w)" );
      for ( auto l = nLevels - 1; l >= 0; --l )
      {
         const auto task = thread * nLevels + l;
         const auto taskBegin = taskBoundaries[ task + 1 ];
         const auto taskEnd = taskBoundaries[ task ];

         PRAGMA( forceinline )
         P2PWait( l, parentIndexBackward, parentsBackward, taskFinishedBackward );

         for ( auto i = taskBegin - 1; i >= taskEnd; --i )
         {
            PA v = static_cast<PA>( y[ i ] );

            for ( auto jIdx = uRowIndexLevelScheduling[ i ] + 1; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               const auto j = uJColLevelScheduling[ jIdx ];
               v -= uAluLevelScheduling[ jIdx ] * static_cast<PA>( y[ j ] );
            }

            y[ i ] = static_cast<double>( uAluLevelScheduling[ uRowIndexLevelScheduling[ i ] ] * v );
         }

         PRAGMA( forceinline )
         P2PNotify( task, taskFinishedBackward );
      }
      timer.pop();
   }

   template<typename setupPrecision>
   ALWAYS_INLINE void NumericalSetup( const int64 * lRowIndex, const int64* uRowIndex, int minPivot,
      int n, const int * jCol, setupPrecision * alu )
   {
      // Temporary array for handling indices of nonzero entries in the row being calculated.
      // jIndex[ j ] gives the index in alu where the entry in j-th column is.
      // jIndex[ j ] = -1 when j-th column is not part of the LU factors.
      std::vector<int64> jIndexArr( n, -1 );
      int64* jIndex = &( jIndexArr[ 0 ] );

      // Algorithm follows ikj version of Gaussian elimination: each pass in the i-loop
      // calculates i-th row of L and U factors, by eliminating all previous processed
      // rows (k-loop). Elimination of k-th row involves a linear combination between rows
      // i and k (j-loop). See, for instance, section 10.3.1 of Saad's book Iterative
      // Methods for Sparse Linear Systems, Second Edition.

      // Loop in i is the outermost one, fully calculating one row in each pass
      for ( int i = 0; i < n; i++ )
      {
         // Starting and final indices in alu and jCol for entries in the i-th row.
         // Pointers for L and U parts for the same row are handled by different arrays.
         int64 lRowBegin = lRowIndex[ i ];
         int64 lRowEnd   = lRowIndex[ i + 1 ];

         int64 uRowBegin = uRowIndex[ n - i - 1 ];
         int64 uRowEnd   = uRowIndex[ n - i ];

         // Zeroes out i-th row of alu and sets jIndex[ j ] to the index in alu where the entry in j-th column is.
         for ( auto kk = lRowBegin; kk < lRowEnd; kk++ )
         {
            jIndex[ jCol[ kk ] ] = kk;
         }
         for ( auto kk = uRowBegin; kk < uRowEnd; kk++ )
         {
            jIndex[ jCol[ kk ] ] = kk;
         }

         // No elimination required for the first row
         if ( i > 0 )
         {
            // Loop in k: eliminate previous rows
            for (auto kk = lRowBegin; kk < lRowEnd; kk++)
            {
               int k = jCol[ kk ];

               // Multiplier for eliminating row k is a(i,k)/a(k,k). Note that alu stores 1/a(k,k).
               // Note also that the L factor is made of those multipliers and therefore they have to be copied back to alu.
               setupPrecision mult = alu[ jIndex[ k ] ] * alu[ uRowIndex[ n - k - 1 ] ];
               alu[ jIndex[ k ] ] = mult;

               // Loop in j: (sparse) linear combination between rows i and k.
               // In Matlab notation: a(i,k+1:n) = a(i,k+1:n) - mult*a(k,k+1:n), where mult is the multiplier.
               for ( auto jj = uRowIndex[ n - k - 1 ] + 1; jj < uRowIndex[ n - k ]; jj++ )
               {
                  const int j = jCol[ jj ];

                  // Only update a(i,j) if it is to be retained according to the level of fill criteria.
                  if ( jIndex[ j ] != -1 )
                  {
                     alu[ jIndex[ j ] ] -= mult * alu[ jj ];
                  }
               } // j-loop
            } // k-loop
         }
         // Reset jIndex entries to -1 to be used when processing next row.
         for ( auto kk = lRowBegin; kk < lRowEnd; kk++ )
         {
            jIndex[ jCol[ kk ] ] = -1;
         }
         for ( auto kk = uRowBegin; kk < uRowEnd; kk++ )
         {
            jIndex[ jCol[ kk ] ] = -1;
         }
         // alu stores the inverse of the diagonal for the U factor
         setupPrecision diagU = alu[ uRowBegin ];
         if ( diagU == static_cast<setupPrecision>( 0.0 ) )
         {
            throw Exception( "ILUCore: Zero pivot in row " + std::to_string( i ) + "." );
         }
         else if( std::abs( diagU ) < minPivot )
         {
            diagU = diagU > 0 ? minPivot : -minPivot;
         }
         alu[ uRowBegin ] = static_cast<setupPrecision>( 1.0 ) / diagU;

      } // i-loop: this is the end of elimination algorithm
   }

   template<typename setupPrecision>
   ALWAYS_INLINE void NumericalSetupBarrierLevelScheduling( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int minPivot,
      int64 * jIndex, const int * taskBoundaries,
      int nLevels, int thread, int n,
      const int * jColLevelScheduling, setupPrecision * aluLevelScheduling )
   {
      for ( int l = 0; l < nLevels; ++l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l + 1 ];

         // Algorithm follows ikj version of Gaussian elimination: each pass in the i-loop
         // calculates i-th row of L and U factors, by eliminating all previous processed
         // rows (k-loop). Elimination of k-th row involves a linear combination between rows
         // i and k (j-loop). See, for instance, section 10.3.1 of Saad's book Iterative
         // Methods for Sparse Linear Systems, Second Edition.

         // Loop in i is the outermost one, fully calculating one row in each pass
         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            // Starting and final indices in alu and jCol for entries in the i-th row.
            // Pointers for L and U parts for the same row are handled by different arrays.
            int64 lRowBegin = lRowIndexLevelScheduling[ i ];
            int64 lRowEnd   = lRowIndexLevelScheduling[ i + 1 ];

            int64 uRowBegin = uRowIndexLevelScheduling[ i ];
            int64 uRowEnd   = uRowIndexLevelScheduling[ i + 1 ];

            // Zeroes out i-th row of alu and sets jIndex[ j ] to the index in alu where the entry in j-th column is.
            for ( auto kk = lRowBegin; kk < lRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = kk;
            }
            for ( auto kk = uRowBegin; kk < uRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = kk;
            }

            // No elimination required for the first row
            if ( i > 0 )
            {
               // Loop in k: eliminate previous rows
               for (auto kk = lRowBegin; kk < lRowEnd; kk++)
               {
                  int k = jColLevelScheduling[ kk ];

                  // Multiplier for eliminating row k is a(i,k)/a(k,k). Note that alu stores 1/a(k,k).
                  // Note also that the L factor is made of those multipliers and therefore they have to be copied back to alu.
                  setupPrecision mult = aluLevelScheduling[ jIndex[ k ] ] * aluLevelScheduling[ uRowIndexLevelScheduling[ k ] ];
                  aluLevelScheduling[ jIndex[ k ] ] = mult;

                  // Loop in j: (sparse) linear combination between rows i and k.
                  // In Matlab notation: a(i,k+1:n) = a(i,k+1:n) - mult*a(k,k+1:n), where mult is the multiplier.
                  for ( auto jj = uRowIndexLevelScheduling[ k ] + 1; jj < uRowIndexLevelScheduling[ k + 1 ]; jj++ )
                  {
                     const int j = jColLevelScheduling[ jj ];

                     // Only update a(i,j) if it is to be retained according to the level of fill criteria.
                     if ( jIndex[ j ] != -1 )
                     {
                        aluLevelScheduling[ jIndex[ j ] ] -= mult * aluLevelScheduling[ jj ];
                     }
                  } // j-loop
               } // k-loop
            }
            // Reset jIndex entries to -1 to be used when processing next row.
            for ( auto kk = lRowBegin; kk < lRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = -1;
            }
            for ( auto kk = uRowBegin; kk < uRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = -1;
            }
            // alu stores the inverse of the diagonal for the U factor
            setupPrecision diagU = aluLevelScheduling[ uRowBegin ];
            if ( diagU == static_cast<setupPrecision>( 0.0 ) )
            {
               throw Exception( "ILUCore: Zero pivot in row " + std::to_string( i ) + "." );
            }
            else if( std::abs( diagU ) < minPivot )
            {
               diagU = diagU > 0 ? minPivot : -minPivot;
            }
            aluLevelScheduling[ uRowBegin ] = static_cast<setupPrecision>( 1.0 ) / diagU;
         } // i-loop: this is the end of elimination algorithm
         OpenMP::Barrier();
      }
   }

   template<typename setupPrecision>
   ALWAYS_INLINE void NumericalSetupP2PLevelScheduling( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int minPivot,
      int64 * jIndex, const int * taskBoundaries,
      const int * parentIndexForward, const int * parentsForward,
      int nLevels, int thread, int nThreads, int n,
      const int * jColLevelScheduling, setupPrecision * aluLevelScheduling )
   {
      boost::shared_ptr< SharedArray< int > > taskFinishedForwardSharedArray;
      CreateTaskFinished( nLevels, nThreads, taskFinishedForwardSharedArray );
      volatile int * taskFinishedForward = taskFinishedForwardSharedArray->begin();

      for ( int l = 0; l < nLevels; ++l )
      {
         const int task = thread * nLevels + l;
         const int taskBegin = taskBoundaries[ task ];
         const int taskEnd = taskBoundaries[ task + 1 ];

         PRAGMA( forceinline )
         P2PWait( l, parentIndexForward, parentsForward, taskFinishedForward );

         // Algorithm follows ikj version of Gaussian elimination: each pass in the i-loop
         // calculates i-th row of L and U factors, by eliminating all previous processed
         // rows (k-loop). Elimination of k-th row involves a linear combination between rows
         // i and k (j-loop). See, for instance, section 10.3.1 of Saad's book Iterative
         // Methods for Sparse Linear Systems, Second Edition.

         // Loop in i is the outermost one, fully calculating one row in each pass
         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            // Starting and final indices in alu and jCol for entries in the i-th row.
            // Pointers for L and U parts for the same row are handled by different arrays.
            int64 lRowBegin = lRowIndexLevelScheduling[ i ];
            int64 lRowEnd   = lRowIndexLevelScheduling[ i + 1 ];

            int64 uRowBegin = uRowIndexLevelScheduling[ i ];
            int64 uRowEnd   = uRowIndexLevelScheduling[ i + 1 ];

            // Zeroes out i-th row of alu and sets jIndex[ j ] to the index in alu where the entry in j-th column is.
            for ( auto kk = lRowBegin; kk < lRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = kk;
            }
            for ( auto kk = uRowBegin; kk < uRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = kk;
            }

            // No elimination required for the first row
            if ( i > 0 )
            {
               // Loop in k: eliminate previous rows
               for (auto kk = lRowBegin; kk < lRowEnd; kk++)
               {
                  int k = jColLevelScheduling[ kk ];

                  // Multiplier for eliminating row k is a(i,k)/a(k,k). Note that alu stores 1/a(k,k).
                  // Note also that the L factor is made of those multipliers and therefore they have to be copied back to alu.
                  setupPrecision mult = aluLevelScheduling[ jIndex[ k ] ] * aluLevelScheduling[ uRowIndexLevelScheduling[ k ] ];
                  aluLevelScheduling[ jIndex[ k ] ] = mult;

                  // Loop in j: (sparse) linear combination between rows i and k.
                  // In Matlab notation: a(i,k+1:n) = a(i,k+1:n) - mult*a(k,k+1:n), where mult is the multiplier.
                  for ( auto jj = uRowIndexLevelScheduling[ k ] + 1; jj < uRowIndexLevelScheduling[ k + 1 ]; jj++ )
                  {
                     const int j = jColLevelScheduling[ jj ];

                     // Only update a(i,j) if it is to be retained according to the level of fill criteria.
                     if ( jIndex[ j ] != -1 )
                     {
                        aluLevelScheduling[ jIndex[ j ] ] -= mult * aluLevelScheduling[ jj ];
                     }
                  } // j-loop
               } // k-loop
            }
            // Reset jIndex entries to -1 to be used when processing next row.
            for ( auto kk = lRowBegin; kk < lRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = -1;
            }
            for ( auto kk = uRowBegin; kk < uRowEnd; kk++ )
            {
               jIndex[ jColLevelScheduling[ kk ] ] = -1;
            }
            // alu stores the inverse of the diagonal for the U factor
            setupPrecision diagU = aluLevelScheduling[ uRowBegin ];
            if ( diagU == static_cast<setupPrecision>( 0.0 ) )
            {
               throw Exception( "ILUCore: Zero pivot in row " + std::to_string( i ) + "." );
            }
            else if( std::abs( diagU ) < minPivot )
            {
               diagU = diagU > 0 ? minPivot : -minPivot;
            }
            aluLevelScheduling[ uRowBegin ] = static_cast<setupPrecision>( 1.0 ) / diagU;
         } // i-loop: this is the end of elimination algorithm

         PRAGMA( forceinline )
         P2PNotify( task, taskFinishedForward );
      }
   }

   template<typename PA>
   ALWAYS_INLINE void AluInitLevelScheduling( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * taskBoundaries, int nLevels,
      int thread, PA * aluLevelScheduling )
   {
      for ( int l = 0; l < nLevels; ++l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l + 1 ];

         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               aluLevelScheduling[ jIdx ] = static_cast<PA>( 0.0 );
            }
         }
      }

      for ( int l = nLevels - 1; l >= 0; --l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l + 1 ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l ];

         for ( int i = taskBegin - 1; i >= taskEnd; --i )
         {
            for ( auto jIdx = uRowIndexLevelScheduling[ i ]; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               aluLevelScheduling[ jIdx ] = static_cast<PA>( 0.0 );
            }
         }
      }
   }

   template<typename PA>
   ALWAYS_INLINE void AluInitLevelSchedulingJCol( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * jCol, const int * taskBoundaries, int nLevels,
      int thread, PA * aluLevelScheduling )
   {
      for ( int l = 0; l < nLevels; ++l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l + 1 ];

         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               auto j = jCol[ jIdx ];
               aluLevelScheduling[ jIdx ] += static_cast<PA>( j );
            }
         }
      }

      for ( int l = nLevels - 1; l >= 0; --l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l + 1 ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l ];

         for ( int i = taskBegin - 1; i >= taskEnd; --i )
         {
            for ( auto jIdx = uRowIndexLevelScheduling[ i ]; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               auto j = jCol[ jIdx ];
               aluLevelScheduling[ jIdx ] += static_cast<PA>( j );
            }
         }
      }
   }

   template<typename PA>
   ALWAYS_INLINE void AluInitLevelSchedulingJColUZeroIdx( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * lJCol, const int * uJCol, const int * taskBoundaries, int nLevels,
      int thread, PA * lAluLevelScheduling, PA * uAluLevelScheduling )
   {
      for ( int l = 0; l < nLevels; ++l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l + 1 ];

         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               auto j = lJCol[ jIdx ];
               lAluLevelScheduling[ jIdx ] += static_cast<PA>( j );
            }
         }
      }

      for ( int l = nLevels - 1; l >= 0; --l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l + 1 ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l ];

         for ( int i = taskBegin - 1; i >= taskEnd; --i )
         {
            for ( auto jIdx = uRowIndexLevelScheduling[ i ]; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               auto j = uJCol[ jIdx ];
               uAluLevelScheduling[ jIdx ] += static_cast<PA>( j );
            }
         }
      }
   }

   template<typename FROM_PRECISION, typename TO_PRECISION>
   ALWAYS_INLINE void AluConvertPrecisionLevelScheduling( const int64 * lRowIndexLevelScheduling,
      const int64 * uRowIndexLevelScheduling, const int * taskBoundaries, int nLevels,
      int thread, FROM_PRECISION * aluLevelScheduling, TO_PRECISION * aluConvertedLevelScheduling )
   {
      for ( int l = 0; l < nLevels; ++l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l + 1 ];

         for ( int i = taskBegin; i < taskEnd; ++i )
         {
            for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               aluConvertedLevelScheduling[ jIdx ] = static_cast<TO_PRECISION>( aluLevelScheduling[ jIdx ] );
            }
         }
      }

      for ( int l = nLevels - 1; l >= 0; --l )
      {
         const int taskBegin = taskBoundaries[ thread * nLevels + l + 1 ];
         const int taskEnd = taskBoundaries[ thread * nLevels + l ];

         for ( int i = taskBegin - 1; i >= taskEnd; --i )
         {
            for ( auto jIdx = uRowIndexLevelScheduling[ i ]; jIdx < uRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
            {
               aluConvertedLevelScheduling[ jIdx ] = static_cast<TO_PRECISION>( aluLevelScheduling[ jIdx ] );
            }
         }
      }
   }
}

namespace solver {
   namespace linear {
      namespace solver {
         namespace preconditioner {

            template <typename precision, typename setupPrecision> ILUCore<precision, setupPrecision>::ILUCore() :
               m_minPivot( 1.0e-16 )
            {
               m_options.put( m_minPivotStr, 1.0e-16 );

               // Set the precision string according to the template argument
               // (The precision option is handled in ILUCoreFactory as the instantiation of the preconditioner depends on the option's value.)
               if ( typeid( precision ) == typeid( double ) )
               {
                  m_options.put( m_strPrecision, "double" );
                  m_precision = "double";
               }
               else if ( typeid( precision ) == typeid( float ) )
               {
                  m_options.put( m_strPrecision, "single" );
                  m_precision = "single";
               }
               if ( typeid( setupPrecision ) == typeid( double ) )
               {
                  m_options.put( m_strSetupPrecision, "double" );
                  m_setupPrecision = "double";
               }
               else if ( typeid( setupPrecision ) == typeid( float ) )
               {
                  m_options.put( m_strSetupPrecision, "single" );
                  m_setupPrecision = "single";
               }

               if ( ! ::utils::debug::GetEnv( "SBR_VERBOSE" ).empty() )
               {
                  _COUT( "[ILUCore] ILU preconditioner setup was instantiated with " << m_setupPrecision << " precision." );
                  _COUT( "[ILUCore] ILU preconditioner apply was instantiated with " << m_precision << " precision." );
               }
            }

            template <typename precision, typename setupPrecision> bool ILUCore<precision, setupPrecision>::validateOption( const std::string& key, boost::any value )
            {
               if ( !key.compare( "Min Pivot" ) )
               {
                  m_minPivot = boost::any_cast<double>( value );
                  if ( m_minPivot <= 0 )
                  {
                     throw Exception( "The parameter Min Pivot must be a positive number." );
                  }
                  return true;
               }

               if ( !key.compare( m_strPrecision ) )
               {
                  std::string arg = boost::any_cast<std::string>( value );

                  if ( arg != m_precision )
                     throw Exception( "[ILUCore] The parameter 'precision' is already set (to '" + m_precision + "') and cannot be modified after the instantiation of the preconditioner." );

                  return true;
               }

               if ( !key.compare( m_strSetupPrecision ) )
               {
                  std::string arg = boost::any_cast<std::string>( value );

                  if ( arg != m_setupPrecision )
                     throw Exception( "[ILUCore] The parameter 'setupPrecision' is already set (to '" + m_setupPrecision + "') and cannot be modified after the instantiation of the preconditioner." );

                  return true;
               }

               return ILUCoreBase::validateOption( key, value );
            }

            template <typename precision, typename setupPrecision> boost::shared_ptr< CoreDDAlgorithm > ILUCore<precision, setupPrecision>::cloneConfigs() const
            {
               boost::shared_ptr< CoreDDAlgorithm > clone( new ILUCore() );
               OptionsUtils::CopyOptionsToCoreAlgorithm( m_options, clone );
               return clone;
            }

            template <typename precision, typename setupPrecision> void ILUCore<precision, setupPrecision>::symbolicSetup( const boost::shared_ptr< MatrixExtractor >& matExtractor )
            {
               // Invoke common algorithm for symbolic setup
               symbolicSetupCore( matExtractor );

               const auto n = matExtractor->size();
               if ( n > m_jIndexArr.size() )
               {
                  m_jIndexArr.resize( n, -1 );
               }

               if ( m_isLevelScheduling )
                  m_aluLevelScheduling = SharedArray<setupPrecision>::Factory::Allocate::New( m_jColLevelScheduling->size() );
            }

            template <typename precision, typename setupPrecision> void ILUCore<precision, setupPrecision>::aluInit( double value, int blockSize )
            {
               const int thread = OpenMP::ThreadId();
               const auto * taskBoundaries = m_taskBoundaries->begin();
               const auto * origToThreadContPerm = m_origToThreadContPerm->begin();
               const auto * threadContToOrigPerm = m_threadContToOrigPerm->begin();

               const auto * lRowIndexLevelScheduling = m_lRowIndexLevelScheduling->begin();
               const auto * uRowIndexLevelScheduling = m_uRowIndexLevelScheduling->begin();
               const auto * uRowIndexLevelSchedulingZeroIdx = m_uRowIndexLevelSchedulingZeroIdx->begin();

               const auto * lJColLevelScheduling = m_lJColLevelScheduling->begin();
               auto * lAluLevelScheduling = m_lAluLevelScheduling->begin();

               const auto * uJColLevelScheduling = m_uJColLevelScheduling->begin();
               auto * uAluLevelScheduling = m_uAluLevelScheduling->begin();

               //AluInitLevelSchedulingJCol( lRowIndexLevelScheduling, uRowIndexLevelScheduling, jColLevelScheduling,
               //   taskBoundaries, m_nLevels, thread, aluLevelScheduling );

               AluInitLevelSchedulingJColUZeroIdx( lRowIndexLevelScheduling, uRowIndexLevelSchedulingZeroIdx,
                  lJColLevelScheduling, uJColLevelScheduling,
                  taskBoundaries, m_nLevels, thread, lAluLevelScheduling, uAluLevelScheduling );
            }

            template <typename precision, typename setupPrecision> void ILUCore<precision, setupPrecision>::numericalSetup( const boost::shared_ptr< MatrixExtractor >& matExtractor )
            {
               TimerTree& timer = GlobalTimerTree::Instance();

               const int n = matExtractor->size();
               int64* aluStrBlockIndx = nullptr;

               if ( n > 0 )
               {
                  if ( m_isLevelScheduling )
                  {
                     boost::shared_ptr< MatrixExtractorForILU > matExtractorILU =
                        boost::dynamic_pointer_cast< MatrixExtractorForILU >( matExtractor );
                     CK_TRUE( matExtractorILU );

                     const int thread = OpenMP::ThreadId();
                     const int nThreads = OpenMP::NumThreads();

                     m_aluLevelScheduling = SharedArray<setupPrecision>::Factory::Allocate::New( m_jColLevelScheduling->size() );

                     const int * taskBoundaries = m_taskBoundaries->begin();
                     const int * origToThreadContPerm = m_origToThreadContPerm->begin();
                     const int * threadContToOrigPerm = m_threadContToOrigPerm->begin();

                     const int64 * lRowIndexLevelScheduling = m_lRowIndexLevelScheduling->begin();
                     int64 * uRowIndexLevelScheduling = m_uRowIndexLevelScheduling->begin();

                     const int * jColLevelScheduling = m_jColLevelScheduling->begin();
                     setupPrecision * aluLevelScheduling = m_aluLevelScheduling->begin();

                     AluInitLevelScheduling( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                        taskBoundaries, m_nLevels, thread, aluLevelScheduling );

                     auto * jIndex = m_jIndexArr.data();

                     // Copy ooriginal matrix into alu
                     matExtractorILU->extractMatrixForNumFact( taskBoundaries[ m_nLevels * thread ],
                        taskBoundaries[ m_nLevels * ( thread + 1 ) ],
                        jIndex, lRowIndexLevelScheduling,
                        uRowIndexLevelScheduling, jColLevelScheduling, aluLevelScheduling );

                     if ( m_isP2P )
                     {
                        const int * parentIndexForward = m_parentIndexForward.data();
                        const int * parentsForward = m_parentsForward.data();
   
                        timer.push( "ILUCore Numerical Setup" );
                        NumericalSetupP2PLevelScheduling( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                           m_minPivot, jIndex,
                           taskBoundaries, parentIndexForward, parentsForward, m_nLevels, thread, nThreads, n,
                           jColLevelScheduling, aluLevelScheduling );
                        OPENMP( omp barrier );
                        timer.pop();
                     }
                     else
                     {
                        timer.push( "ILUCore Numerical Setup" );
                        NumericalSetupBarrierLevelScheduling( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                           m_minPivot, jIndex,
                           taskBoundaries, m_nLevels, thread, n, jColLevelScheduling, aluLevelScheduling );
                        timer.pop();
                     }

                     // Convert to the final precision (no copy when there is no mixed-precision)
                     if ( typeid( m_aluConvertedLevelScheduling ) != typeid( m_aluLevelScheduling ) )
                     {
                        m_aluConvertedLevelScheduling = std::move( SharedArray<precision>::Factory::Allocate::New( m_aluLevelScheduling->size() ) );
                        precision * aluConv = m_aluConvertedLevelScheduling->begin();

                        AluConvertPrecisionLevelScheduling<setupPrecision, precision>( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                           taskBoundaries, m_nLevels, thread, aluLevelScheduling, aluConv );
                     }

                     // Separate jCol and alu
                     // First adjust uRowIndexLevelScheduling to start from zero index
                     m_uRowIndexLevelSchedulingZeroIdx = std::move( SharedArray<int64>::Factory::Allocate::New( n + 1 ) );
                     auto * uRowIndexLevelSchedulingZeroIdx = m_uRowIndexLevelSchedulingZeroIdx->begin();
                     auto uOffset = uRowIndexLevelScheduling[ 0 ];

                     auto threadBeginRow = taskBoundaries[ ( thread + 1 ) * m_nLevels ];
                     auto threadEndRow = taskBoundaries[ thread * m_nLevels ];

                     auto beginOffset = thread == nThreads - 1 ? 0 : 1;
                     for ( auto i = threadBeginRow - beginOffset; i >= threadEndRow; --i )
                     {
                        uRowIndexLevelSchedulingZeroIdx[ i ] = uRowIndexLevelScheduling[ i ] - uOffset;
                     }

                     OPENMP( omp barrier )

                     m_lJColLevelScheduling = std::move( SharedArray<int>::Factory::Allocate::New( uRowIndexLevelSchedulingZeroIdx[ n ] ) );
                     m_uJColLevelScheduling = std::move( SharedArray<int>::Factory::Allocate::New( uRowIndexLevelSchedulingZeroIdx[ n ] ) );
                     m_lAluLevelScheduling = std::move( SharedArray<precision>::Factory::Allocate::New( uRowIndexLevelSchedulingZeroIdx[ n ] ) );
                     m_uAluLevelScheduling = std::move( SharedArray<precision>::Factory::Allocate::New( uRowIndexLevelSchedulingZeroIdx[ n ] ) );

                     auto * lJColLevelScheduling = m_lJColLevelScheduling->begin();
                     auto * uJColLevelScheduling = m_uJColLevelScheduling->begin();
                     auto * lAluLevelScheduling = m_lAluLevelScheduling->begin();
                     auto * uAluLevelScheduling = m_uAluLevelScheduling->begin();

                     const precision * aluLS = typeid( m_aluConvertedLevelScheduling ) == typeid( m_aluLevelScheduling ) ?
                        reinterpret_cast<const precision*>( m_aluLevelScheduling->begin() ) : m_aluConvertedLevelScheduling->begin();

                     for ( int l = 0; l < m_nLevels; ++l )
                     {
                        const auto taskBegin = taskBoundaries[ thread * m_nLevels + l ];
                        const auto taskEnd = taskBoundaries[ thread * m_nLevels + l + 1 ];

                        for ( auto i = taskBegin; i < taskEnd; ++i )
                        {
                           for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
                           {
                              lJColLevelScheduling[ jIdx ] = jColLevelScheduling[ jIdx ];
                           }
                        }
                     }

                     for ( auto l = m_nLevels - 1; l >= 0; --l )
                     {
                        const auto taskBegin = taskBoundaries[ thread * m_nLevels + l + 1 ];
                        const auto taskEnd = taskBoundaries[ thread * m_nLevels + l ];

                        for ( auto i = taskBegin - 1; i >= taskEnd; --i )
                        {
                           for ( auto jIdx = uRowIndexLevelSchedulingZeroIdx[ i ]; jIdx < uRowIndexLevelSchedulingZeroIdx[ i + 1 ]; ++jIdx )
                           {
                              uJColLevelScheduling[ jIdx ] = jColLevelScheduling[ jIdx + uOffset ];
                           }
                        }
                     }

                     for ( int l = 0; l < m_nLevels; ++l )
                     {
                        const auto taskBegin = taskBoundaries[ thread * m_nLevels + l ];
                        const auto taskEnd = taskBoundaries[ thread * m_nLevels + l + 1 ];

                        for ( auto i = taskBegin; i < taskEnd; ++i )
                        {
                           for ( auto jIdx = lRowIndexLevelScheduling[ i ]; jIdx < lRowIndexLevelScheduling[ i + 1 ]; ++jIdx )
                           {
                              lAluLevelScheduling[ jIdx ] = aluLS[ jIdx ];
                           }
                        }
                     }

                     for ( auto l = m_nLevels - 1; l >= 0; --l )
                     {
                        const auto taskBegin = taskBoundaries[ thread * m_nLevels + l + 1 ];
                        const auto taskEnd = taskBoundaries[ thread * m_nLevels + l ];

                        for ( auto i = taskBegin - 1; i >= taskEnd; --i )
                        {
                           for ( auto jIdx = uRowIndexLevelSchedulingZeroIdx[ i ]; jIdx < uRowIndexLevelSchedulingZeroIdx[ i + 1 ]; ++jIdx )
                           {
                              uAluLevelScheduling[ jIdx ] = aluLS[ jIdx + uOffset ];
                           }
                        }
                     }

                     OPENMP( omp barrier )
                  }
                  else
                  {
                     boost::shared_ptr< RowExtractorForILU > rowExtractor = boost::dynamic_pointer_cast< RowExtractorForILU >( matExtractor );
                     CK_TRUE( rowExtractor );

                     // Dereference pointers to the arrays storing the LU factors for performance reasons
                     int* jCol = &( m_jCol[ 0 ] );
                     int64* lRowIndex = &( m_lRowIndex[ 0 ] );
                     int64* uRowIndex = &( m_uRowIndex[ 0 ] );

                     m_alu.resize( m_jCol.size(), static_cast<setupPrecision>( 0.0 ) );
                     setupPrecision* alu = &( m_alu[ 0 ] );
                     int n = rowExtractor->size();
                     rowExtractor->extractMatrixForNumFact( lRowIndex, uRowIndex, aluStrBlockIndx, jCol, alu );

                     timer.push( "ILUCore Numerical Setup" );
                     NumericalSetup( lRowIndex, uRowIndex, m_minPivot, n, jCol, alu );
                     timer.pop();

                     // Convert to the final precision (no copy when there is no mixed-precision)
                     if ( typeid( m_aluConverted ) != typeid( m_alu ) )
                        m_aluConverted = std::vector<precision>( m_alu.begin(), m_alu.end() );
                  }
               }
            }

            template <typename precision, typename setupPrecision> void ILUCore<precision, setupPrecision>::apply( double* y, const double* x, const int n, const int blockSize, const int* eqStrIndx ) const
            {
               if ( n > 0 )
               {
                  TimerTree& timer = GlobalTimerTree::Instance();

                  if ( m_isLevelScheduling )
                  {
                     const int64 * lRowIndexLevelScheduling =  m_lRowIndexLevelScheduling->begin();;
                     const int64 * uRowIndexLevelScheduling =  m_uRowIndexLevelScheduling->begin();;
                     const int * jColLevelScheduling = m_jColLevelScheduling->begin();

                     const auto * uRowIndexLevelSchedulingZeroIdx =  m_uRowIndexLevelSchedulingZeroIdx->begin();;
                     const auto * lJColLevelScheduling = m_lJColLevelScheduling->begin();
                     const auto * uJColLevelScheduling = m_uJColLevelScheduling->begin();

                     // When not using mixed-precision, m_aluConvertedLevelScheduling is not touched and thus we must use the original m_aluLevelScheduling.
                     // The reinterpret_cast does nothing here (no-op), just prevent compilation error when the types differ.
                     const precision* aluLevelScheduling = typeid( m_aluConvertedLevelScheduling ) == typeid( m_aluLevelScheduling ) ?
                        reinterpret_cast<const precision*>( m_aluLevelScheduling->begin() ) : m_aluConvertedLevelScheduling->begin();

                     const auto * lAluLevelScheduling = m_lAluLevelScheduling->begin();;
                     const auto * uAluLevelScheduling = m_uAluLevelScheduling->begin();;

                     const int * threadContToOrigPerm = m_threadContToOrigPerm->begin();
                     const int * taskBoundaries = m_taskBoundaries->begin();

                     const int nThreads = OpenMP::NumThreads();
                     const int thread = OpenMP::ThreadId();

                     if ( m_isP2P )
                     {
                        const int * parentIndexForward = m_parentIndexForward.data();
                        const int * parentsForward = m_parentsForward.data();

                        const int * parentIndexBackward = m_parentIndexBackward.data();
                        const int * parentsBackward = m_parentsBackward.data();

                        if ( true )
                        {
                           PRAGMA( forceinline )
                           ApplyP2PLevelSchedulingUIdx( lRowIndexLevelScheduling, uRowIndexLevelSchedulingZeroIdx,
                              lJColLevelScheduling, uJColLevelScheduling, lAluLevelScheduling, uAluLevelScheduling, taskBoundaries,
                              parentIndexForward, parentsForward, parentIndexBackward, parentsBackward,
                              n, m_nLevels, thread, nThreads,
                              y, x, timer );

                        }
                        else
                        {
                           PRAGMA( forceinline )
                           ApplyP2PLevelScheduling( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                              jColLevelScheduling, aluLevelScheduling, taskBoundaries,
                              parentIndexForward, parentsForward, parentIndexBackward, parentsBackward,
                              n, m_nLevels, thread, nThreads,
                              y, x, timer );

                        }
                        PRAGMA( forceinline )
                        ApplyP2PLevelScheduling( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                           jColLevelScheduling, aluLevelScheduling, taskBoundaries,
                           parentIndexForward, parentsForward, parentIndexBackward, parentsBackward,
                           n, m_nLevels, thread, nThreads,
                           y, x, timer );
                     }
                     else
                     {
                        PRAGMA( forceinline )
                        ApplyBarrierLevelScheduling( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                           jColLevelScheduling, aluLevelScheduling, taskBoundaries, n, m_nLevels, thread, nThreads,
                           y, x, timer );
                     }
                  }
                  else
                  {
                     // Dereference pointers to the arrays storing the LU factors for performance reasons
                     const int* jCol = &( m_jCol[ 0 ] );
                     const int64* lRowIndex = &( m_lRowIndex[ 0 ] );
                     const int64* uRowIndex = &( m_uRowIndex[ 0 ] );

                     // When not using mixed-precision, m_aluConverted is not touched and thus we must use the original m_alu.
                     // The reinterpret_cast does nothing here (no-op), just prevent compilation error when the types differ.
                     const precision* alu = typeid( m_aluConverted ) == typeid( m_alu ) ? reinterpret_cast<const precision*>( m_alu.data() ) : m_aluConverted.data();
                     // For ILU, applying operator is given by y = [ U^(-1) * L^(-1) ] * x
                     // This is obtained by solving (LU)*y = x, which is done in two steps:
                     // 1 - Solve L*w = x
                     // 2 - Solve U*y = w

                     // Solve L*w = x, w is stored in y (forward solution)
                     timer.push( "ILUCore Solve (L * w = x)" );
                     y[ 0 ] = x[ 0 ];
                     for ( int row = 1; row < n; row++ )
                     {
                        precision v = static_cast<precision>( x[ row ] );
                        for ( auto j = lRowIndex[ row ]; j < lRowIndex[ row + 1 ]; j++ )
                        {
                           v -= alu[ j ] * static_cast<precision>( y[ jCol[ j ] ] );
                        }
                        y[ row ] = static_cast<double>( v );
                     }
                     timer.pop();

                     // Solve U*y = w (backward solution)
                     timer.push( "ILUCore Solve (U * y = w)" );
                     y[ n - 1 ] *= static_cast<double>( alu[ uRowIndex[ 0 ] ] );
                     for ( int row = n - 2; row >= 0; row-- )
                     {
                        precision v = static_cast<precision>( y[ row ] );
                        for ( auto j = uRowIndex[ n - row - 1 ] + 1; j < uRowIndex[ n - row ]; j++ )
                        {
                           v -= alu[ j ] * static_cast<precision>( y[ jCol[ j ] ] );
                        }
                        y[ row ] = static_cast<double>( alu[ uRowIndex[ n - row - 1 ] ] * v );
                     }
                     timer.pop();
                  }
               }
            }

            template <typename precision, typename setupPrecision> void ILUCore<precision, setupPrecision>::luMatvec( double* y, const double* x, const int n, const int blockSize, const int* eqStrIndx ) const
            {
               if ( n > 0 )
               {
                  if ( m_isLevelScheduling )
                  {
                     const int * threadContToOrigPerm = m_threadContToOrigPerm->begin();
                     const int * taskBoundaries = m_taskBoundaries->begin();
                     const int thread = OpenMP::ThreadId();

                     const int* jColLevelScheduling = m_jColLevelScheduling->begin();
                     const int64* lRowIndexLevelScheduling = m_lRowIndexLevelScheduling->begin();
                     const int64* uRowIndexLevelScheduling = m_uRowIndexLevelScheduling->begin();

                     double* aluLevelScheduling;
                     SharedArray<double> aluDoubleLevelScheduling = std::move( SharedArray<double>::Factory::Allocate::Instance( m_aluLevelScheduling->size() ) );
                     if ( typeid( setupPrecision ) != typeid( double ) ) // Convert from double to single only if necessary
                     {
                        aluLevelScheduling = aluDoubleLevelScheduling.begin();
                        setupPrecision* aluLevelSchedulingSetupPrecision = m_aluLevelScheduling->begin();
                        AluConvertPrecisionLevelScheduling<setupPrecision, double>( lRowIndexLevelScheduling, uRowIndexLevelScheduling,
                           taskBoundaries, m_nLevels, thread, aluLevelSchedulingSetupPrecision, aluLevelScheduling );
                     }
                     else
                     {
                        aluLevelScheduling = reinterpret_cast<double*>( m_aluLevelScheduling->begin() ); // The reinterpret_cast does nothing here (no-op), just prevent compilation error when the types differ.
                     }

                     // y = (L*U)*x is calculated in two steps: w = U*x and y = L*w
                     // w = U*x. Just for convinience we iterate levels as we do at bwd solve
                     for ( int l = m_nLevels - 1; l >= 0; --l )
                     {
                        const int taskBegin = taskBoundaries[ thread * m_nLevels + l + 1 ];
                        const int taskEnd = taskBoundaries[ thread * m_nLevels + l ];

                        for ( int row = taskBegin - 1; row >= taskEnd; --row )
                        {
                           // Start by calculating w = diag(U)*x. Recall that (diag(U))^(-1) is stored.
                           double v = x[ row ] / aluLevelScheduling[ uRowIndexLevelScheduling[ row ] ];

                           // Calculates off-diagonal contribution for w = U*x
                           for ( auto j = uRowIndexLevelScheduling[ row ] + 1; j < uRowIndexLevelScheduling[ row + 1 ]; ++j )
                           {
                              v += aluLevelScheduling[ j ] * x[ jColLevelScheduling[ j ] ];
                           }
                           y[ row ] = v;
                        }
                     }

                     // Assure w = U*x is fully calculated
                     OpenMP::Barrier();

                     // Calculates L*w = y. w is stored in y.
                     // Loop from last level to first level in order to avoid
                     // changing y entries which are needed in subsequent passes of the loop
                     for ( int l = m_nLevels - 1; l >= 0; --l )
                     {
                        const int taskBegin = taskBoundaries[ thread * m_nLevels + l + 1 ];
                        const int taskEnd = taskBoundaries[ thread * m_nLevels + l ];

                        for ( int row = taskBegin - 1; row >= taskEnd; --row )
                        {
                           double v = y[ row ];
                           for ( auto j = lRowIndexLevelScheduling[ row ]; j < lRowIndexLevelScheduling[ row + 1 ]; ++j )
                           {
                              v += aluLevelScheduling[ j ] * y[ jColLevelScheduling[ j ] ];
                           }
                           y[ row ] = v;
                        }
                        OpenMP::Barrier();
                     }
                  }
                  else
                  {
                     // Dereference pointers to the arrays storing the LU factors for performance reasons
                     const int* jCol = &( m_jCol[ 0 ] );
                     const int64* lRowIndex = &( m_lRowIndex[ 0 ] );
                     const int64* uRowIndex = &( m_uRowIndex[ 0 ] );
                     const double* alu;

                     std::unique_ptr< std::vector<double> > m_aluDouble;
                     if ( typeid( setupPrecision ) != typeid( double ) ) // Convert from double to single only if necessary
                     {
                        m_aluDouble.reset( new std::vector<double>( m_alu.begin(), m_alu.end() ) );
                        alu = m_aluDouble->data();
                     }
                     else
                     {
                        alu = reinterpret_cast<const double*>( m_alu.data() ); // The reinterpret_cast does nothing here (no-op), just prevent compilation error when the types differ.
                     }

                     // y = (L*U)*x is calculated in two steps: w = U*x and y = L*w
                     // Start by calculating w = diag(U)*x. Recall that (diag(U))^(-1) is stored.
                     for ( int row = 0; row < n; row++ )
                     {
                        y[ row ] = x[ row ] / alu[ uRowIndex[ n - row - 1 ] ];
                     }
                     // Calculates off-diagonal contribution for w = U*x
                     for ( int row = n - 2; row >= 0; row-- )
                     {
                        double v = y[ row ];
                        for ( auto j = uRowIndex[ n - row - 1 ] + 1; j < uRowIndex[ n - row ]; j++ )
                        {
                           v += alu[ j ] * x[ jCol[ j ] ];
                        }
                        y[ row ] = v;
                     }
                     // Calculates L*w = y. w is stored in y, loop from n to 1 in order to avoid
                     // changing y entries which are needed in subsequent passes of the loop
                     for ( int row = n - 1; row > 0; row-- )
                     {
                        double v = y[ row ];  // Recall that L has unit diagonal
                        for ( auto j = lRowIndex[ row ]; j < lRowIndex[ row + 1 ]; j++ )
                        {
                           v += alu[ j ] * y[ jCol[ j ] ];
                        }
                        y[ row ] = v;
                     }
                  }
               }
            }

            // Explicit template instantiation
            template class ILUCore<double,double>;
            template class ILUCore<double,float>;
            template class ILUCore<float,double>;
            template class ILUCore<float,float>;
         }
      }
   }
}
