#!/bin/bash

module load gcc/6.5
source /opt/intel/parallel_studio_xe_2018/compilers_and_libraries_2018.5.274/linux/bin/compilervars.sh intel64
source /opt/intel/parallel_studio_xe_2019/vtune_amplifier/amplxe-vars.sh

prefix=$1
shift
suffix=$1
shift
size=$1
shift
bind1=$1
shift
nthreads=$1
shift
analysis=$1
shift

export OMP_PLACES=cores
export OMP_PROC_BIND=$bind1

cd /petrobr/parceirosbr/solverbrict/joao.ramirez2/ilu-lvl-sched/singleMultidomain/intel/vtune/${suffix}

dirname="${prefix}-${size}-${nthreads}nthrds-${analysis}-${bind1}-${suffix}-2019"

if [ ! -d $dirname ]
then
   mkdir $dirname
fi

cd $dirname

if [ ! -f ILULvlSchedTest ]
then
   ln -s $(pwd)/../ILULvlSchedTestIntel18 ILULvlSchedTest
fi

if [ ! -f Kernels.json ]
then
   sed "s/\@\@size\@\@/${size}/g" $(pwd)/../Kernels.json.size.nthreads | sed "s/\@\@nthreads\@\@/${nthreads}/g" | tee Kernels.json 
fi

time amplxe-cl  -collect $analysis -app-working-dir=$(pwd) -finalization-mode=full -data-limit=1000 -result-dir=$dirname -search-dir=$(pwd) -source-search-dir=/petrobr/parceirosbr/solverbrict/joao.ramirez2/solver/solverbr-ilu-lvl-sched/trunk/Source -user-data-dir=$(pwd)/../results ./ILULvlSchedTest &> ${dirname}.log 
