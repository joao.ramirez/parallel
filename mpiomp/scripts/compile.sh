#!/bin/bash

if [ ! -d ../../build ]; then
   mkdir ../../build
fi

NREP=40
NCALC=1
NEQS=2147483647

for comp in gnu intel; do
   ccompiler=gcc
   cxxcompiler=g++

   if [ "$comp" == "intel" ]; then
      ccompiler=icc
      cxxcompiler=icc
   fi
   
   if [ ! -d ../../build/${comp} ]; then
      mkdir ../../build/${comp}
   fi
   
   cd ../../build/${comp}

   echo "Building for ${ccompiler} at $(pwd)"
   echo "cmake -G "Unix Makefiles" -DCMAKE_C_COMPILER=${ccompiler}  -DCMAKE_CXX_COMPILER=${cxxcompiler} -D_ADD_MPIOMP=ON -D_NREP=${NREP} -D_NCALC=${NCALC} -D_NEQS=${NEQS} $(readlink -e ../..)"
   
   cmake -G "Unix Makefiles" -DCMAKE_C_COMPILER=${ccompiler}  -DCMAKE_CXX_COMPILER=${cxxcompiler} -D_ADD_MPIOMP=ON  -D_NREP=${NREP} -D_NCALC=${NCALC} -D_NEQS=${NEQS} ../..
   make
   cd -
done
