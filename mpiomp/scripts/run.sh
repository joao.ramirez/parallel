#!/bin/bash

# Assumes binaries are in build/intel and build/gcc
# all paths used here are relative to project root

# Assumes 40 cores and no HT
# MPI used in a single node and one process per socket
# MPI is assumed to be Intel MPI
mpiexec --version

export I_MPI_PIN_DOMAIN=socket
echo "I_MPI_PIN_DOMAIN=${I_MPI_PIN_DOMAIN}"

export OMP_PLACES=cores
echo "OMP_PLACES=${OMP_PLACES}"

for comp in gnu intel; do

   echo "====== $comp run ======"
   
   echo "___________"
   echo "|          |"
   echo "| OMP TASK |"
   echo "|__________|"
   
   # Omp task
   export OMP_NUM_THREADS=20
   echo "OMP_NUM_THREADS=${OMP_NUM_THREADS}"
   
   for bind in spread close; do
      echo "OMP_PROC_BIND=${bind}"
      export OMP_PROC_BIND=${bind}
      mpiexec -n 2 ../../build/${comp}/mpiomp/mpiomptask
   done
   
   echo "_____________"
   echo "|            |"
   echo "| OMP NESTED |"
   echo "|____________|"
   
   #Omp nested
   export OMP_NUM_THREADS=20,20
   export OMP_NESTED=TRUE
   echo "OMP_NUM_THREADS=${OMP_NUM_THREADS}"
   echo "OMP_NESTED=${OMP_NUM_THREADS}"
   
   export KMP_HOT_TEAMS_MODE=1
   export KMP_HOT_TEAMS_MAX_LEVEL=2
   echo "KMP_HOT_TEAMS_MODE=${KMP_HOT_TEAMS_MODE}"
   echo "KMP_HOT_TEAMS_MAX_LEVEL=${KMP_HOT_TEAMS_MAX_LEVEL}"
   
   for bind1 in spread close; do
      for bind2 in spread close; do
         echo "OMP_PROC_BIND=${bind1},${bind2}"
         export OMP_PROC_BIND=${bind1},${bind2}
         mpiexec -n 2 ../../build/${comp}/mpiomp/mpiompnested
      done
   done
   
   echo "====== $comp run ======"
done
