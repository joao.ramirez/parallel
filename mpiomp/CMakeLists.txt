option( _NREP "Number of repetitions of omp task and nested benchmarks" 10 )
option( _NCALC "Number of times the main calculation (creating nested parallel region or issuing a taskloop) is performed" 10 )
option( _NEQS "Size of arrays used to perform the calculation" 1000 )

add_definitions( -D_NREP=${_NREP} -D_CALC=${_NCALC} -D_NEQS=${_NEQS} )
message( STATUS "_NREP = ${_NREP}, _NCALC=${_NCALC}, _NEQS=${_NEQS}" )

find_package( OpenMP REQUIRED )
message( STATUS "OpenMP is ${OPENMP_FOUND}" )

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER}" )

find_package( MPI REQUIRED )
message( STATUS "MPI is ${MPI_FOUND}. Version is ${MPI_VERSION}" )
message( STATUS "MPI compiler is ${MPI_CXX_COMPILER}" )

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MPI_C_COMPILE_OPTIONS}" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MPI_CXX_COMPILE_OPTIONS}" )
#set( CMAKE_EXE_LINKER_FLAGS "${MPI_CXX_LINK_FLAGS}" )

message( STATUS "C Compiler flags after MPI/OpenMP are: ${CMAKE_C_FLAGS}" )
message( STATUS "CXX Compiler flags after MPI/OpenMP are: ${CMAKE_CXX_FLAGS}" )
message( STATUS "Linker flags are: ${CMAKE_EXE_LINKER_FLAGS}" )

add_executable( mpiomptask mpiomptask.cpp )
add_executable( mpiompnested mpiompnested.cpp )

include_directories( mpiomptask ${MPI_INCLUDE_PATH} )
include_directories( mpiompnested ${MPI_INCLUDE_PATH} )

target_link_libraries( mpiomptask ${MPI_LIBRARIES} )
target_link_libraries( mpiompnested ${MPI_LIBRARIES} )
