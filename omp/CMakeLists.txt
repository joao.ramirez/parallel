find_package( OpenMP REQUIRED )

option( _NREP "Number of repetitions of omp task and nested benchmarks" 10 )
option( _NCALC "Number of times the main calculation (creating nested parallel region or issuing a taskloop) is performed" 10 )
option( _NEQS "Size of arrays used to perform the calculation" 1000 )

add_definitions( -D_NREP=${_NREP} -D_CALC=${_NCALC} -D_NEQS=${_NEQS} )

message( STATUS "OpenMP is ${OPENMP_FOUND}" )
message( STATUS "_NREP = ${_NREP}, _NCALC=${_NCALC}, _NEQS=${_NEQS}" )

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER}" )

message( STATUS "C Compiler flags after OpenMP are: ${CMAKE_C_FLAGS}" )
message( STATUS "CXX Compiler flags after OpenMP are: ${CMAKE_CXX_FLAGS}" )
message( STATUS "Linker flags are: ${CMAKE_EXE_LINKER_FLAGS}" )

add_executable( omptask omptask.cpp )
add_executable( ompnested ompnested.cpp )
