#include <iostream>
#include <utility>
#include <cmath>
#include <omp.h>

//  _      _   
// | |    | |   
// | |    | |   
// |_|\   |_|   
// | | \  | |   
// | |  \ | |   
// |_|   \|_|   
// | |    | |   
// | |    | |   
// |_|    |_|  

#ifndef _NREP
#define _NREP 1
#endif

#ifndef _NCALC
#define _NCALC 1
#endif

#ifndef _NEQS
#define _NEQS 100
#endif

#define ASSERT( a, b, e ) if ( std::abs( a - b ) > e ) std::cout << a << " != " << b << std::endl

int main()
{
    constexpr int nRep = _NREP;
    constexpr int nCalc = _NCALC;
    constexpr size_t nEqs = _NEQS;
    constexpr int nDomains = 2;

    std::cout << "NREP, NCALC, NEQS = " << nRep << ", " << nCalc << ", " << nEqs << std::endl;

    constexpr size_t domSize = nEqs / nDomains;
    constexpr size_t domRem = nEqs % nDomains;
 
    size_t domIdx[ nDomains + 1 ];
    for ( int i = 0, domStrIdx = 0; i < nDomains + 1; ++i, domStrIdx += domSize )
    {
        domIdx[ i ] = domStrIdx;
    }
    domIdx[ nDomains ] += domRem;

    double *v = new double[ nEqs ];
    
    double *times = new double[ nDomains ];
    for ( int i = 0; i < nDomains; ++i )
    {
        times[ i ] = 0.0;
    }

    double t1 = omp_get_wtime();

    #pragma omp parallel
    {
        int thread = omp_get_thread_num();
        int nThreads = omp_get_num_threads();
        double *v1, *v2;

        if ( thread == 0 || thread == 1 )
        {
            size_t tDomSize = domIdx[ thread + 1 ] - domIdx[ thread ]; 
            v1 = new double[ tDomSize ];
            v2 = new double[ tDomSize ];

            for ( auto i = domIdx[ thread ]; i < domIdx[ thread + 1 ]; ++i )
            {
                v[ i ] = ( double ) thread / 100.0;
                v2[ i - domIdx[ thread ] ] = ( ( double ) thread + 2 ) / 100.0;
            }
        }

        #pragma omp barrier

        for ( int k = 0; k < nRep; ++k )
        {
            if ( thread == 0 || thread == 1 )
            {
                auto tDomSize = domIdx[thread + 1] - domIdx[thread];

                for ( auto i = domIdx[ thread ]; i < domIdx[ thread + 1 ]; ++i )
                {
                    v1[ i - domIdx[ thread ] ] = v[ i ];
                }

                double tini = omp_get_wtime();

                for ( int j = 0; j < nCalc; ++j )
                {
                    #pragma omp taskloop grainsize(tDomSize / 5 * nThreads) firstprivate(tDomSize, v1, v2)
                    for ( size_t i = 0; i < tDomSize; ++i )
                    {
                        v1[ i ] += v2[ i ];
                    }
                }

                times[ thread ] += omp_get_wtime() - tini;
            }

            #pragma omp barrier

            if ( thread == 0 || thread == 1 )
            {

                int globalDomIdxForShift = ( thread + 1 ) % nDomains;

                for ( auto i = domIdx[ globalDomIdxForShift  ]; i < domIdx[ globalDomIdxForShift + 1 ]; ++i )
                {
                    v[ i ] = v1[ i - domIdx[ globalDomIdxForShift ] ];
                }
            }

            #pragma omp barrier
        }

        if ( thread == 0 || thread == 1 )
        {
            delete v1; delete v2;
        }
    }

    double t2 = omp_get_wtime();
    std::cout << "Total Elapsed time is  " << t2 - t1 << std::endl;

    double avgtime = 0.0;
    double maxtime = 0.0;
    double mintime = times[ 0 ];
    for ( int i = 0; i < nDomains; ++i )
    {
        avgtime += times[ i ];
        maxtime = times[ i ] > maxtime ? times[ i ] : maxtime;
        mintime = times[ i ] < mintime ? times[ i ] : mintime;
    }

    std::cout << "Avg Calc Elapsed time is (sec.) " << avgtime / ( double ) nDomains << std::endl;
    std::cout << "Max Calc Elapsed time is (sec.) " << maxtime << std::endl;
    std::cout << "Min Calc Elapsed time is (sec.) " << mintime << std::endl;

#ifdef _DEBUG
    for ( int i = 0; i < nEqs; ++i )
    {
        std::cout << "v[ " << i << " ] = " << v[ i ] << std::endl;
    }
#endif

    // Correctness
    double a = 0.00, b = 0.01;
    for ( int i = 0; i < nRep; ++i )
    {
        for ( int j = 0; j < nCalc; ++j )
        {
            a += 0.02 ;
            b += 0.03 ;
            std::cout << "Depois. a = " << a << ", b = " << b << std::endl;
        }
        std::swap( a, b );
    }

    for ( size_t i = 0; i < nEqs; ++i )
    {
        if ( i < nEqs / 2 )
        {
            ASSERT( v [ i ], a, 1e-5 );
        }
        else
        {
            ASSERT( v [ i ], b, 1e-5 );
        }
    }

    delete v;

    return 0;
}
