#include <iostream>
#include <omp.h>
#include <mutex>
#include <condition_variable>

int main()
{
    const int nThreads = 16;
    const int groups[] = {0, 4, 8, 16};
    int curgroupidx = 0;
    int threadgroupcount = groups[curgroupidx];
    bool threadgroupbarrier = false;

    std::condition_variable condVar;

    // This mutex implements a copyprivate action on variable a.
    std::mutex m;

    // This mutex implements thread group ordering execution
    std::mutex mg;

    // This mutex implements a thread group barrier.
    std::mutex mt;

    #pragma omp parallel num_threads(nThreads) 
    {
        int i = 0;
        while (omp_get_thread_num() >= groups[i + 1]) ++i;
        int mgroup = i;
        int mgroupSize = groups[i + 1] - groups[i];

        int *a;
        static int *aShared;

        // ********************************************************************
        // This ensures first group proceed without waiting
        if (groups[0] != groups[mgroup])
        {
            std::unique_lock<std::mutex> glk (mg);
            condVar.wait(glk, [&] () {
                return groups[curgroupidx] == groups[mgroup];
	        });
        }
        // ********************************************************************

        // ---------------------------------------------------------------------
        // Global allocation and copyprivate within thread group
        // Variable aShared is used as global allocation variable
        {
            std::unique_lock<std::mutex> lk(m);
            if (omp_get_thread_num() == groups[curgroupidx])
            {
                #pragma omp flush (aShared)
                aShared = new int {omp_get_thread_num() * 2};

                a = aShared;
                lk.unlock();

                condVar.notify_all();
            }
            else
            {
                if (aShared == nullptr)
                {
                    condVar.wait(lk, [&] () {
                        return aShared != nullptr;
                    });
                }

                a = aShared;
            }
        }
        // ---------------------------------------------------------------------

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
        // All threads from current group must get the corrected allocated address
        // in order to let the next thread group perform its allocation
        {
            std::unique_lock<std::mutex> tlk(mt);

            if (++threadgroupcount < mgroupSize)
            {
                condVar.wait(tlk, [&] () {return threadgroupbarrier;});

                if (--threadgroupcount == 0)
                {
                    threadgroupbarrier = false;
                    tlk.unlock();

                    // Allow next group to proceed
                    std::unique_lock<std::mutex> glk(mg);

                    ++curgroupidx;
                    #pragma omp flush (aShared)
                    aShared = nullptr;

                    glk.unlock();
                    condVar.notify_all();
                }
            }
            else
            {
                threadgroupbarrier = true;
                --threadgroupcount;
                tlk.unlock();
                condVar.notify_all();
            }
        }
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        #pragma omp critical
        std::cout << "t" << omp_get_thread_num() << ", a = " << a << ", *a = " << *a << std::endl;
   }

   return 0; 
}
